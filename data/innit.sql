CREATE SCHEMA jdbc;
CREATE TABLE jdbc.movie
(
    id             SERIAL NOT NULL PRIMARY KEY,
    title          varchar(255),
    year           varchar(4),
    score          int,
    comment        varchar(255),
    IMDB           varchar(8),
    description    varchar(1000)
);