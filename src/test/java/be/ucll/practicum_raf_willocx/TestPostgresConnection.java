package be.ucll.practicum_raf_willocx;

import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class TestPostgresConnection {
    @Test
    void name() throws SQLException {
        Connection connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "mysecretpassword");
        String databaseName = connection.getMetaData().getDatabaseProductName();
        System.out.println(databaseName);
    }
}
