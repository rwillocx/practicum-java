package be.ucll.practicum_raf_willocx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticumRafWillocxApplication {

	public static void main(String[] args) {

		SpringApplication.run(PracticumRafWillocxApplication.class, args);
	}

}
