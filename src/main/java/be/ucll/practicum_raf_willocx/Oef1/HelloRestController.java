package be.ucll.practicum_raf_willocx.Oef1;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloRestController {

    @GetMapping("/hello")
    public String hello()
    {
        return "Hello, world!";
    }
    @GetMapping(value = "/hello", params = "naam")
    public String hello(@RequestParam("naam") String naam)
    {
        return "Hello, " + naam;
    }
}
