package be.ucll.practicum_raf_willocx.Oef2;

import be.ucll.practicum_raf_willocx.Oef3.JsonReader;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Repository
public class MovieDAO {
    public static final String INSERT_SQL = "INSERT INTO jdbc.movie (title, year, score, comment, imdb, description) VALUES (?,?,?,?,?,?)";
    public static final String UPDATE_SQL = "UPDATE jdbc.movie SET  year = ?, score = ?, comment = ?, imdb= ?, description= ? WHERE id = ?;";
    public static final String DELETE_SQL = "DELETE FROM jdbc.movie WHERE id = ?;";
    public static final String SELECT_ALL_SQL = "SELECT * FROM jdbc.movie;";
    public static final String FIND_BY_ID_SQL = "SELECT * FROM jdbc.movie WHERE id = ?;";
    public static final String FIND_BY_TITLE_SQL = "SELECT * FROM jdbc.movie WHERE title = ?;";



    private final Connection connection;


    public MovieDAO() throws SQLException {
        this.connection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres",
                "postgres",
                "mysecretpassword");
    }

    public Movie create(Movie movie) throws SQLException, IOException, JSONException {
        String api_url = "http://www.omdbapi.com/?t=" + movie.getTitle() + "&apikey=c772768d";
        JSONObject json = JsonReader.readJsonFromUrl(api_url);
        PreparedStatement preparedStatement = connection.prepareStatement(INSERT_SQL);
        preparedStatement.setString(1, movie.getTitle());
        preparedStatement.setString(2, movie.getYear());
        preparedStatement.setInt(3, movie.getScore());
        preparedStatement.setString(4, movie.getComment());
        preparedStatement.setString(5, json.getString("imdbRating"));
        preparedStatement.setString(6, json.getString("Plot"));
        preparedStatement.execute();
        return movie;
    }

    public Movie update(Movie movie, int id) throws SQLException, IOException, JSONException {
        String api_url = "http://www.omdbapi.com/?t=" + movie.getTitle() + "&apikey=c772768d";
        JSONObject json = JsonReader.readJsonFromUrl(api_url);
        PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL);
        preparedStatement.setString(1, movie.getYear());
        preparedStatement.setInt(2, movie.getScore());
        preparedStatement.setString(3, movie.getComment());
        preparedStatement.setString(4,json.getString("imdbRating"));
        preparedStatement.setString(5,json.getString("Plot"));
        preparedStatement.setInt(6, id);
        preparedStatement.execute();
        return movie;
    }

    public void delete(int id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL);
        preparedStatement.setInt(1, id);
        preparedStatement.execute();
    }

    public List<Movie> getAll() throws SQLException {
        List<Movie> movies = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_SQL);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            movies.add(new Movie.Builder()
                    .title(resultSet.getString("title"))
                    .year(resultSet.getString("year"))
                    .score(resultSet.getInt("score"))
                    .comment(resultSet.getString("comment"))
                    .imdb(resultSet.getString("imdb"))
                    .description(resultSet.getString("description"))
                    .build());
        }
        return movies;
    }

    public Optional<Movie> findByMovieId(int id) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            return Optional.of(new Movie.Builder()
                    .title(resultSet.getString("title"))
                    .year(resultSet.getString("year"))
                    .score(resultSet.getInt("score"))
                    .comment(resultSet.getString("comment"))
                    .imdb(resultSet.getString("imdb"))
                    .description(resultSet.getString("description"))
                    .build());
        }
        return Optional.empty();
    }

    public Optional<Movie> findByMovieTitle(String title) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_TITLE_SQL);
        preparedStatement.setString(1, title);
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next()) {
            return Optional.of(new Movie.Builder()
                    .title(resultSet.getString("title"))
                    .year(resultSet.getString("year"))
                    .score(resultSet.getInt("score"))
                    .comment(resultSet.getString("comment"))
                    .imdb(resultSet.getString("imdb"))
                    .description(resultSet.getString("description"))
                    .build());
        }
        return Optional.empty();
    }





}
