package be.ucll.practicum_raf_willocx.Oef2;

import be.ucll.practicum_raf_willocx.Oef3.JsonReader;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.http.HttpClient;
import java.time.LocalDate;

public class Movie {
    private String title;
    private String year;
    private int score;
    private String comment;
    private String IMDB;
    private String samenvatting;



    public Movie(String title, String year, int score, String comment) throws IOException, JSONException {
        this.title = title;
        this.year = year;
        this.score = score;
        this.comment = comment;
    }

    public Movie() {
    }

    public Movie(Builder builder) {
        setTitle(builder.title);
        setYear(builder.year);
        setScore(builder.score);
        setComment(builder.comment);
        setIMDB(builder.IMDB);
        setSamenvatting(builder.description);
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public int getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    public String getIMDB() {
        return IMDB;
    }

    public String getSamenvatting() {
        return samenvatting;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setIMDB(String IMDB) {
        this.IMDB = IMDB;
    }

    public void setSamenvatting(String samenvatting) {
        this.samenvatting = samenvatting;
    }

    public static final class Builder {
        private String title;
        private String year;
        private int score;
        private String comment;
        private String IMDB;
        private String description;

        public Builder() {
        }

        public Builder(Movie copy) {
            this.title = copy.getTitle();
            this.year = copy.getYear();
            this.score = copy.getScore();
            this.comment = copy.getComment();
            this.IMDB = copy.getIMDB();
            this.description = copy.getSamenvatting();

        }

        public Builder title(String val) {
            this.title = val;
            return this;
        }

        public Builder year(String val) {
            this.year = val;
            return this;
        }

        public Builder score(int val) {
            this.score = val;
            return this;
        }

        public Builder comment(String val) {
            this.comment = val;
            return this;
        }

        public Builder imdb(String val)
        {
            this.IMDB = val;
            return this;
        }

        public Builder description(String val)
        {

            this.description = val;
            return this;
        }


        public Movie build() {
            return new Movie(this);
        }
    }
}
