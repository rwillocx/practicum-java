package be.ucll.practicum_raf_willocx.Oef2;

import be.ucll.practicum_raf_willocx.Oef3.JsonReader;
import org.json.JSONException;
import org.json.JSONObject;
import org.postgresql.core.SqlCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/films")
public class MovieRestController {
@Autowired
private MovieDAO movieDAO;

@PostMapping
    public Movie create(@RequestBody Movie movie) throws SQLException, IOException, JSONException {
    String api_url = "http://www.omdbapi.com/?t=" + movie.getTitle() + "&apikey=c772768d";
    JSONObject json = JsonReader.readJsonFromUrl(api_url);
    movie.setIMDB(json.getString("imdbRating"));
    movie.setSamenvatting(json.getString("Plot"));
    return movieDAO.create(movie);
}
@GetMapping
    public Optional<Movie> getMovie(@RequestParam("title") String title) throws SQLException {
    return movieDAO.findByMovieTitle(title);
}

@GetMapping("/all")
public List<Movie> listAll() throws SQLException {
    return movieDAO.getAll();
}


@GetMapping("/{id}")
    public Optional<Movie> getMovieById(@PathVariable("id") int id) throws SQLException {
    return movieDAO.findByMovieId(id);
}

@DeleteMapping("/{id}")
    public void delete(@PathVariable("id") int id) throws SQLException {
    movieDAO.delete(id);
}

@PutMapping("/{id}")
    public void put(@PathVariable("id") int id, @RequestParam("title") String title, @RequestParam("year") String year, @RequestParam("score") int score, @RequestParam("comment") String comment) throws SQLException, IOException, JSONException {
    Movie update = new Movie(title,year, score,comment);
    movieDAO.update(update, id);
}




}
